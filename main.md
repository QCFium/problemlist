QCFiumが原案を出した問題リスト  
  

難易度のABCは令和ABC基準  

| 公開日時 | コンテスト | 問題 | 難易度 | 備考 |
| ---- | ---- | ---- | ---- | ---- |
| 2019-09-13 | [yukicoder contest 223](https://yukicoder.me/contests/233) |[約数倍数](https://yukicoder.me/problems/no/882) | ABC 150 | 
| 2019-09-13 | [yukicoder contest 223](https://yukicoder.me/contests/233) | [ぬりえ](https://yukicoder.me/problems/no/883) | AGC 400 | 
| 2019-09-13 | [yukicoder contest 223](https://yukicoder.me/contests/233) | [Eat and Add](https://yukicoder.me/problems/no/884) | ABC 400 | 
| 2019-09-13 | [yukicoder contest 223](https://yukicoder.me/contests/233) | [アマリクエリ](https://yukicoder.me/problems/no/885) | ABC 600 | 
| 2019-09-13 | [yukicoder contest 223](https://yukicoder.me/contests/233) | [Direct](https://yukicoder.me/problems/no/886) | AGC 700 | 
| 2019-09-23 | [Kodamanと愉快な仲間たち](https://www.hackerrank.com/contests/kodamanwithothers) | [No common Points](https://www.hackerrank.com/contests/kodamanwithothers/challenges/no-common-points) | AGC 600 | 面白いからお願いだからOEIS使わないで
| 2019-09-23 | [Kodamanと愉快な仲間たち](https://www.hackerrank.com/contests/kodamanwithothers) | [Xor max](https://www.hackerrank.com/contests/kodamanwithothers/challenges/xor-max) | ABC 600 | [既出](https://yukicoder.me/problems/no/130)
| 2019-12-04 | [yukicoder Advent Calender Contest 2019](https://yukicoder.me/contests/245) | [商とあまり](https://yukicoder.me/problems/no/941) | AGC 1300+ | 
| 2019-12-13 | [2019冬講習コン #1](https://oj.fuwa.dev/contests/1) | [2D reverse](https://oj.fuwa.dev/problems/3) | ABC 250 | 
| 2019-12-13 | [2019冬講習コン #1](https://oj.fuwa.dev/contests/1) | [Text editor](https://oj.fuwa.dev/problems/5) | JOI 7 | ほとんど使われないかわいそうなSTLに活躍の場を
| 2019-12-13 | [2019冬講習コン #1](https://oj.fuwa.dev/contests/1) | [ビル跳び](https://oj.fuwa.dev/problems/4) | JOI 11 | これと同じクオリティのを二度と作れる気がしない
| 2019-12-13 | [2020冬旅行コン](https://oj.fuwa.dev/contests/2) | [strcmp](https://oj.fuwa.dev/problems/9) | ABC 100 | 既出覚悟
| 2019-12-13 | [2020冬旅行コン](https://oj.fuwa.dev/contests/2) | [Separation](https://oj.fuwa.dev/problems/13) | ABC 300 | 既出覚悟
| 2019-12-13 | [2020冬旅行コン](https://oj.fuwa.dev/contests/2) | [Xth root](https://oj.fuwa.dev/problems/14) | ABC 300 | 既出覚悟
| 2019-12-13 | [2020冬旅行コン](https://oj.fuwa.dev/contests/2) | [Multiple count](https://oj.fuwa.dev/problems/15) | JOI 6 | 
| 2019-12-13 | [2020冬旅行コン](https://oj.fuwa.dev/contests/2) | [Dynamic connectivity](https://oj.fuwa.dev/problems/16) | JOI 11 | 教育。
| 2019-12-06 | [JOI2019 予選模試](https://www.hackerrank.com/contests/joi2019) | [ネックレス (Necklace)](https://www.hackerrank.com/contests/joi2019/challenges/challenge-2276) | JOI 7 | 問題文書いたのはE869120
| 2020-02-02 | [セグ木を使うな2020](https://www.hackerrank.com/contests/dont-use-segtree) | [Range chmin query](https://www.hackerrank.com/contests/dont-use-segtree/challenges/range-chmin-query) | JOI 6 | 既出もクソもない
| 2020-02-02 | [セグ木を使うな2020](https://www.hackerrank.com/contests/dont-use-segtree) | [Range assign query](https://www.hackerrank.com/contests/dont-use-segtree/challenges/range-assign-query) | JOI 7 | 既出知ったことか
| 2020-02-02 | [セグ木を使うな2020](https://www.hackerrank.com/contests/dont-use-segtree) | [Range count query](https://www.hackerrank.com/contests/dont-use-segtree/challenges/range-count-query) | JOI 7 | 既出じゃないわけがない
| 2020-02-02 | [セグ木を使うな2020](https://www.hackerrank.com/contests/dont-use-segtree) | [Range index-add/sum query](https://www.hackerrank.com/contests/dont-use-segtree/challenges/range-index-addsum-query) | クソ | 
| 2020-05-04 | [2020文化祭コン](https://oj.fuwa.dev/contests/3) | [Shuffle](https://oj.fuwa.dev/problems/18) | ABC 300 | 
| 2020-05-04 | [2020文化祭コン](https://oj.fuwa.dev/contests/3) | [Multiplied comparison](https://oj.fuwa.dev/problems/19) | ABC 500 | 
| 2020-05-04 | [2020文化祭コン](https://oj.fuwa.dev/contests/3) | [Nice Sum](https://oj.fuwa.dev/problems/20) | ??? 600 | マラソンっぽい
| 2020-05-04 | [2020文化祭コン](https://oj.fuwa.dev/contests/3) | [Memory Allocator](https://oj.fuwa.dev/problems/21) | JOI 11 | 
| 2020-05-16 | [yukicoder contest 248](https://yukicoder.me/contests/263) | [電子機器X](https://yukicoder.me/problems/no/1052) | ABC 300 | 
| 2020-05-16 | [yukicoder contest 248](https://yukicoder.me/contests/263) | [ゲーミング棒](https://yukicoder.me/problems/no/1053) | ABC 400 | 
| 2020-05-16 | [yukicoder contest 248](https://yukicoder.me/contests/263) | [Union add query](https://yukicoder.me/problems/no/1054) | JOI 9 | 
| 2020-05-16 | [yukicoder contest 248](https://yukicoder.me/contests/263) | [牛歩](https://yukicoder.me/problems/no/1055) | ARC 1000 | 
| 2020-05-16 | [yukicoder contest 248](https://yukicoder.me/contests/263) | [2D Lamps](https://yukicoder.me/problems/no/1056) | ARC 900 | 
| 2020-10-25 | [AtCoder PAST 4](https://atcoder.jp/contests/past202010-open/) | [中央値](https://atcoder.jp/contests/past202010-open/tasks/past202010_a) | ABC 100 | 
| 2020-10-25 | [AtCoder PAST 4](https://atcoder.jp/contests/past202010-open/) | [分身](https://atcoder.jp/contests/past202010-open/tasks/past202010_d) | ABC 250 | 
| 2020-10-25 | [AtCoder PAST 4](https://atcoder.jp/contests/past202010-open/) | [アナグラム](https://atcoder.jp/contests/past202010-open/tasks/past202010_e) | ABC 300 | 
| 2020-10-25 | [AtCoder PAST 4](https://atcoder.jp/contests/past202010-open/) | [構文解析](https://atcoder.jp/contests/past202010-open/tasks/past202010_f) | ABC 300 | 
| 2020-10-25 | [AtCoder PAST 4](https://atcoder.jp/contests/past202010-open/) | [村整備](https://atcoder.jp/contests/past202010-open/tasks/past202010_g) | ABC 300 | 
| 2020-12-06 | [yukicoder Advent Calender Contest 2020](https://yukicoder.me/contests/300) | [テスト](https://yukicoder.me/problems/no/1309) | ??? 1300+ | (面倒度 / 見た目の複雑さ)部門優勝
| 2020-12-13 | [AtCoder PAST 5](https://atcoder.jp/contests/past202012-open/) | [ハンコ](https://atcoder.jp/contests/past202012-open/tasks/past202012_e) | ABC 250 | 面倒、盤面そのまま持つ方針採っちゃうと地獄
| 2020-12-13 | [AtCoder PAST 5](https://atcoder.jp/contests/past202012-open/) | [一触即発](https://atcoder.jp/contests/past202012-open/tasks/past202012_f) | ABC 300 | 
| 2020-12-13 | [AtCoder PAST 5](https://atcoder.jp/contests/past202012-open/) | [ヘビ](https://atcoder.jp/contests/past202012-open/tasks/past202012_g) | ABC 300 | 
| 2020-12-13 | [AtCoder PAST 5](https://atcoder.jp/contests/past202012-open/) | [避難計画](https://atcoder.jp/contests/past202012-open/tasks/past202012_i) | ABC 400 | 
| 2020-12-13 | [AtCoder PAST 5](https://atcoder.jp/contests/past202012-open/) | [T消し](https://atcoder.jp/contests/past202012-open/tasks/past202012_l) | ABC 550 | 
| 2020-12-13 | [AtCoder PAST 5](https://atcoder.jp/contests/past202012-open/) | [棒の出荷](https://atcoder.jp/contests/past202012-open/tasks/past202012_m) | ABC 550 | 
| 2020-12-13 | [AtCoder PAST 5](https://atcoder.jp/contests/past202012-open/) | [旅行会社](https://atcoder.jp/contests/past202012-open/tasks/past202012_n) | ABC 550 | 
| 2021-02-20 | [AtCoder Beginner Contest 192](https://atcoder.jp/contests/abc192) | [uNrEaDaBlE sTrInG](https://atcoder.jp/contests/abc192/tasks/abc192_b) | ABC 200 | 
| 2021-02-27 | [キャディコンテスト 2021(AtCoder Beginner Contest 193)](https://atcoder.jp/contests/abc193) | [Discount](https://atcoder.jp/contests/abc193/tasks/abc193_a) | ABC 100 | 
| 2021-03-06 | [AtCoder Beginner Contest 194](https://atcoder.jp/contests/abc194) | [Job Assignment](https://atcoder.jp/contests/abc194/tasks/abc194_b) | ABC 200 | 
| 2021-03-13 | [パナソニックコンテスト(AtCoder Beginner Contest 195)](https://atcoder.jp/contests/abc195) | [Shipping Center](https://atcoder.jp/contests/abc195/tasks/abc195_d) | ABC 400 | 
| 2021-03-27 | [AtCoder Beginner Contest 197(Sponsored by Panasonic)](https://atcoder.jp/contests/abc197) | [Rotate](https://atcoder.jp/contests/abc197/tasks/abc197_a) | ABC 100 | 
| 2021-03-27 | [AtCoder Beginner Contest 197(Sponsored by Panasonic)](https://atcoder.jp/contests/abc197) | [Visibility](https://atcoder.jp/contests/abc197/tasks/abc197_b) | ABC 200 | 

